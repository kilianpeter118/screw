pub mod config;

use std::io::{ self, Write };
use std::time::Duration;
use crossterm::style::Stylize;
use crossterm::{ QueueableCommand, terminal, cursor, event::{ self, KeyCode, KeyModifiers } };
use owo_colors::OwoColorize;

use config::*;

use crate::misc::strip_ansi_codes;
use crate::term::TerminalSize;
use crate::language::lexer::*;
use crate::language::lexer::tokenize::tokenize;

pub enum PromptReturnType {
    String(String),
    CtrlC,
    CtrlD,
}

pub struct PromptState {
    pub history: Vec<String>,
    pub last_exit_code: i32,
}

impl PromptState {
    pub fn new() -> Self {
        Self {
            history: Vec::new(),
            last_exit_code: 0,
        }
    }
}

fn prompt_setup() -> Result<(), io::Error> {
    terminal::enable_raw_mode()?;

    Ok(())
}

fn prompt_cleanup() -> Result<(), io::Error> {
    let mut stdout = io::stdout();

    print!("\n");
    stdout.queue(cursor::MoveToColumn(0))?;
    stdout.flush()?;

    terminal::disable_raw_mode()?;

    Ok(())
}

pub fn prompt(state: &mut PromptState, config: &PromptConfig) -> Result<PromptReturnType, io::Error> {
    prompt_setup()?;

    let mut stdout = io::stdout();

    let mut buffer: Vec<char> = Vec::new();
    let mut cursor_index: usize = 0;

    let mut history_offset: usize = 0;

    loop {
        let evaluated_config = config.evaluate();

        let mut prompt_left: &str = match state.last_exit_code {
            0 => &evaluated_config.prompt_left,
            _ => &evaluated_config.prompt_left_fail,
        };

        let prompt_right: &str = match state.last_exit_code {
            0 => &evaluated_config.prompt_right,
            _ => &evaluated_config.prompt_right_fail,
        };

        let mut prompt_left_len = strip_ansi_codes(prompt_left).chars().count();
        let prompt_right_len = strip_ansi_codes(prompt_right).chars().count();

        let term_size = TerminalSize::get()?;

        let _ = stdout.queue(terminal::SetTitle(evaluated_config.window_title)); // I don't care if setting a window title fails.

        if term_size.rows > 0 {
            stdout.queue(cursor::MoveToColumn(0))?;

            if prompt_left_len + 2 <= term_size.cols as usize && term_size.cols > 0 {
                // DO NOTHING - KEEP LEFT PROMPT THE SAME
            }

            else if term_size.cols == 0 {
                prompt_left = "";
                prompt_left_len = 0;
            }

            else {
                prompt_left = "> ";
                prompt_left_len = 2;
            }

            print!("{}", prompt_left);

            if evaluated_config.color_input == false {
                for c in buffer.iter() {
                    print!("{c}");
                }
            }

            else {
                print_syntax_highlighted_buffer(&buffer, None);
            }

            if buffer.len() + prompt_left_len < (term_size.cols as usize).saturating_sub(prompt_right_len) {
                if prompt_left_len + prompt_right_len + 2 <= term_size.cols as usize {
                    stdout.queue(cursor::MoveToColumn(term_size.cols.saturating_sub(prompt_right_len as u16)))?;

                    print!("{}", prompt_right);
                }
            }

            stdout.queue(cursor::MoveToColumn((prompt_left_len + cursor_index) as u16))?;
        }

        stdout.flush()?;

        if event::poll(Duration::from_millis(100))? {
            if let event::Event::Key(key) = event::read()? {
                match (key.modifiers, key.code) {
                    (KeyModifiers::NONE, KeyCode::Home) => cursor_index = 0,
                    (KeyModifiers::NONE, KeyCode::End) => cursor_index = buffer.len(),
                    (KeyModifiers::NONE, KeyCode::Left) => cursor_index = cursor_index.saturating_sub(1),
                    (KeyModifiers::NONE, KeyCode::Right) => {
                        if cursor_index < buffer.len() {
                            cursor_index += 1;
                        }
                    },
                    (KeyModifiers::NONE, KeyCode::Up) => {
                        if history_offset < state.history.len() {
                            history_offset += 1;

                            buffer = state.history[state.history.len() - history_offset].chars().collect();
                            cursor_index = buffer.len();
                        }
                    },
                    (KeyModifiers::NONE, KeyCode::Down) => {
                        if history_offset > 0 {
                            history_offset -= 1;

                            if history_offset > 0 {
                                buffer = state.history[state.history.len() - history_offset].chars().collect();
                                cursor_index = buffer.len();
                            }

                            else {
                                buffer.clear();
                                cursor_index = 0;
                            }
                        }
                    },
                    (KeyModifiers::NONE, KeyCode::Backspace) => {
                        if cursor_index > 0 {
                            buffer.remove(cursor_index - 1);
                            cursor_index -= 1;
                        }
                    },
                    (KeyModifiers::NONE, KeyCode::Delete) => {
                        if cursor_index != buffer.len() {
                            buffer.remove(cursor_index);
                        }
                    },
                    (KeyModifiers::NONE | KeyModifiers::SHIFT, KeyCode::Char(character)) => {
                        buffer.insert(cursor_index, character);
                        cursor_index += 1;
                    },
                    (KeyModifiers::NONE, KeyCode::Enter) => {
                        break;
                    },
                    (KeyModifiers::CONTROL, KeyCode::Char('c')) => {
                        print!("{}", evaluated_config.ctrlc_display);

                        prompt_cleanup()?;

                        state.last_exit_code = 1;

                        return Ok(PromptReturnType::CtrlC);
                    },
                    (KeyModifiers::CONTROL, KeyCode::Char('d')) => {
                        print!("{}", evaluated_config.ctrld_display);

                        prompt_cleanup()?;

                        return Ok(PromptReturnType::CtrlD);
                    },
                    _ => (),
                };
            }
        }

        if term_size.rows > 0 {
            stdout.queue(terminal::Clear(terminal::ClearType::CurrentLine))?;
            stdout.queue(terminal::Clear(terminal::ClearType::FromCursorDown))?;
        }
    }

    prompt_cleanup()?;

    let buffer_len = buffer.len();

    let string = String::from_iter(buffer);

    if buffer_len > 0 {
        state.last_exit_code = 0; // Assuming the command is successful. (The caller is responsible for changing this.)

        if state.history.len() > 0 {
            if state.history[state.history.len() - 1] != string {
                state.history.push(string.clone());
            }
        }

        else {
            state.history.push(string.clone());
        }
    }

    Ok(PromptReturnType::String(string))
}

fn print_syntax_highlighted_buffer(buffer: &[char], file: Option<String>) {
    let string = String::from_iter(buffer);

    let mut tokens = match tokenize(&string, file) {
        Ok(o) => o,
        Err(_) => TokenStream::new(Vec::new()),
    };

    if tokens.len() == 0 {
        for c in buffer {
            print!("{c}");
        }

        return;
    }

    let mut count: usize = 0;

    while tokens.get(0).unwrap().kind != TokenType::EOF {
        let token = tokens.get(0).unwrap();
        let token_kind = TokenTypeKind::from(token.kind.clone());

        for c in string[count..token.span.end].chars() {
            let (color, bold, italic) = token_kind.prompt_color();

            let mut print_c: String = c.color(color).to_string();

            if bold {
                print_c = print_c.bold().to_string();
            }

            if italic {
                print_c = print_c.italic().to_string();
            }

            print!("{}", print_c);

            count += 1;
        }

        tokens.eat();
    }

    // for (i, c) in buffer.iter().enumerate() {
    //     let c_span = Span::new(i, i + 1);

    //     if tokens.get(0).unwrap().span.is_parent_of(c_span) {
    //         let (color, bold, italic) = TokenTypeKind::from(tokens.get(0).unwrap().kind.clone()).prompt_color();

    //         let mut print_c: String = c.color(color).to_string();

    //         if bold {
    //             print_c = print_c.bold().to_string();
    //         }

    //         if italic {
    //             print_c = print_c.italic().to_string();
    //         }

    //         print!("{}", print_c);
    //     }

    //     else {
    //         tokens.eat();

    //         print!("{c}");
    //     }
    // }
}
