#[inline(always)]
pub fn strip_ansi_codes<S: AsRef<str>>(raw: S) -> String {
    strip_ansi_escapes::strip_str(raw)
}
