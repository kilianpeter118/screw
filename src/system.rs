pub fn username() -> String {
    whoami::username()
}

pub fn hostname() -> String {
    gethostname::gethostname().to_string_lossy().to_string()
}
