> Note: This project is not usable yet! It is still very much under development!

# Screw - A modern terminal shell, for the modern age.

Screw is a shell that aims to do away with the excessive minimalism of other shells like Bash,
while still not being extremely over-complicated like other shells. It provides all the basic
features, as well as some awesome new additions to the terminal workflow!
